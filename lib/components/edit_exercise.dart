import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:numberpicker/numberpicker.dart';
import '../blocs/workouts_cubit.dart';
import '../blocs/workout_cubit.dart';
import '../models/workout.dart';
import '../helpers.dart';

class EditExercise extends StatefulWidget {
  final Workout _workout;
  final int _index;
  final int _exIndex;
  const EditExercise(this._workout, this._index, this._exIndex, {Key key})
      : super(key: key);
  @override
  _EditExerciseState createState() => _EditExerciseState();
}

class _EditExerciseState extends State<EditExercise> {
  TextEditingController _title;

  @override
  void initState() {
    _title = TextEditingController(
        text: widget._workout.exercises[widget._exIndex].title);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Column(children: [
        Row(children: [
          Expanded(
              child: InkWell(
                  onLongPress: () => showDialog(
                      context: context,
                      builder: (_) {
                        final controller = TextEditingController(
                            text: widget
                                ._workout.exercises[widget._exIndex].prelude
                                .toString());
                        return AlertDialog(
                            content: TextField(
                                controller: controller,
                                decoration: InputDecoration(
                                    labelText:
                                        AppLocalizations.of(context).prelude),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ]),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    if (controller.text.isNotEmpty) {
                                      Navigator.pop(context);
                                      setState(() {
                                        widget._workout
                                                .exercises[widget._exIndex] =
                                            widget._workout
                                                .exercises[widget._exIndex]
                                                .copyWith(
                                                    prelude: int.parse(
                                                        controller.text));
                                        BlocProvider.of<WorkoutsCubit>(context)
                                            .saveWorkout(
                                                widget._workout, widget._index);
                                      });
                                    }
                                  },
                                  child:
                                      Text(AppLocalizations.of(context).save))
                            ]);
                      }),
                  child: NumberPicker(
                      itemHeight: 30,
                      value: widget._workout.exercises[widget._exIndex].prelude,
                      minValue: 0,
                      maxValue: 3599,
                      textMapper: (strVal) =>
                          formatTime(int.parse(strVal), false),
                      onChanged: (value) => setState(() {
                            widget._workout.exercises[widget._exIndex] = widget
                                ._workout.exercises[widget._exIndex]
                                .copyWith(prelude: value);
                            BlocProvider.of<WorkoutsCubit>(context)
                                .saveWorkout(widget._workout, widget._index);
                          })))),
          Expanded(
              flex: 3,
              child: TextField(
                  textAlign: TextAlign.center,
                  controller: _title,
                  onChanged: (value) => setState(() {
                        int offset = _title.selection.baseOffset;
                        widget._workout.exercises[widget._exIndex] = widget
                            ._workout.exercises[widget._exIndex]
                            .copyWith(title: value);
                        _title.selection =
                            TextSelection.collapsed(offset: offset);
                        BlocProvider.of<WorkoutsCubit>(context)
                            .saveWorkout(widget._workout, widget._index);
                      }))),
          Expanded(
              child: InkWell(
                  onLongPress: () => showDialog(
                      context: context,
                      builder: (_) {
                        final controller = TextEditingController(
                            text: widget
                                ._workout.exercises[widget._exIndex].duration
                                .toString());
                        return AlertDialog(
                            content: TextField(
                                controller: controller,
                                decoration: InputDecoration(
                                    labelText:
                                        AppLocalizations.of(context).duration),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ]),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    if (controller.text.isNotEmpty) {
                                      Navigator.pop(context);
                                      setState(() {
                                        widget._workout
                                                .exercises[widget._exIndex] =
                                            widget._workout
                                                .exercises[widget._exIndex]
                                                .copyWith(
                                                    duration: int.parse(
                                                        controller.text));
                                        BlocProvider.of<WorkoutsCubit>(context)
                                            .saveWorkout(
                                                widget._workout, widget._index);
                                      });
                                    }
                                  },
                                  child:
                                      Text(AppLocalizations.of(context).save))
                            ]);
                      }),
                  child: NumberPicker(
                      itemHeight: 30,
                      value:
                          widget._workout.exercises[widget._exIndex].duration,
                      minValue: 1,
                      maxValue: 3599,
                      textMapper: (strVal) =>
                          formatTime(int.parse(strVal), false),
                      onChanged: (value) => setState(() {
                            widget._workout.exercises[widget._exIndex] = widget
                                ._workout.exercises[widget._exIndex]
                                .copyWith(duration: value);
                            BlocProvider.of<WorkoutsCubit>(context)
                                .saveWorkout(widget._workout, widget._index);
                          }))))
        ]),
        ButtonBar(alignment: MainAxisAlignment.spaceEvenly, children: [
          (widget._exIndex > 0)
              ? IconButton(
                  icon: const Icon(Icons.arrow_upward),
                  onPressed: () {
                    widget._workout.exercises.insert(widget._exIndex - 1,
                        widget._workout.exercises.removeAt(widget._exIndex));
                    BlocProvider.of<WorkoutCubit>(context)
                        .editExercise(widget._exIndex - 1);
                    BlocProvider.of<WorkoutsCubit>(context)
                        .saveWorkout(widget._workout, widget._index);
                  })
              : null,
          (widget._exIndex < widget._workout.exercises.length - 1)
              ? IconButton(
                  icon: const Icon(Icons.arrow_downward),
                  onPressed: () {
                    widget._workout.exercises.insert(widget._exIndex + 1,
                        widget._workout.exercises.removeAt(widget._exIndex));
                    BlocProvider.of<WorkoutCubit>(context)
                        .editExercise(widget._exIndex + 1);
                    BlocProvider.of<WorkoutsCubit>(context)
                        .saveWorkout(widget._workout, widget._index);
                  })
              : null,
          IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () {
                widget._workout.exercises.removeAt(widget._exIndex);
                BlocProvider.of<WorkoutCubit>(context).editExercise(null);
                BlocProvider.of<WorkoutsCubit>(context)
                    .saveWorkout(widget._workout, widget._index);
              })
        ])
      ]);
}
