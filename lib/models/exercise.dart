import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';

class Exercise extends Equatable {
  final int prelude;
  final String title;
  final int duration;
  final int index;
  final int startTime;

  const Exercise(
      {@required this.prelude,
      @required this.title,
      @required this.duration,
      this.index,
      this.startTime});

  factory Exercise.fromJson(
          Map<String, dynamic> json, int index, int startTime) =>
      Exercise(
          prelude: json['prelude'] as int,
          title: json['title'] as String,
          duration: json['duration'] as int,
          index: index,
          startTime: startTime);

  Map<String, dynamic> toJson() =>
      {'title': title, 'prelude': prelude, 'duration': duration};

  Exercise copyWith(
          {int prelude,
          String title,
          int duration,
          int index,
          int startTime}) =>
      Exercise(
          prelude: prelude ?? this.prelude,
          title: title ?? this.title,
          duration: duration ?? this.duration,
          index: index ?? this.index,
          startTime: startTime ?? this.startTime);

  @override
  List<Object> get props => [title, prelude, duration, index, startTime];

  @override
  bool get stringify => true;
}
