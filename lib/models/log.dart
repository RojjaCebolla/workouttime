import 'package:equatable/equatable.dart';

class Log extends Equatable {
  final int timestamp;
  final String title;

  const Log(this.timestamp, this.title);

  factory Log.fromJson(Map<String, dynamic> json) =>
      Log(json['timestamp'] as int, json['title'] as String);

  Map<String, dynamic> toJson() => {'timestamp': timestamp, 'title': title};

  @override
  List<Object> get props => [timestamp, title];
}
