import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:file_picker/file_picker.dart';
import '../models/workout.dart';
import '../pages/settings_page.dart';
import '../pages/log_page.dart';
import '../helpers.dart';
import '../blocs/workout_cubit.dart';
import '../blocs/workouts_cubit.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text("Workout Time!"), actions: [
        IconButton(
            icon: const Icon(Icons.event_available),
            onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return LogPage();
                  }),
                )),
        IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return SettingsPage();
                  }),
                ))
      ]),
      body: SingleChildScrollView(
          child: BlocBuilder<WorkoutsCubit, List<Workout>>(
              builder: (context, workouts) => ExpansionPanelList.radio(
                  children: workouts
                      .map((workout) => ExpansionPanelRadio(
                          value: workout,
                          headerBuilder: (BuildContext context, bool isExpanded) => ListTile(
                              visualDensity: const VisualDensity(
                                  horizontal: 0,
                                  vertical: VisualDensity.maximumDensity),
                              leading: IconButton(
                                  icon: const Icon(Icons.edit),
                                  onPressed: () =>
                                      BlocProvider.of<WorkoutCubit>(context)
                                          .editWorkout(
                                              workout, workouts.indexOf(workout))),
                              title: Text(workout.title),
                              trailing: Text(formatTime(workout.getTotal(), true)),
                              onTap: () => !isExpanded ? BlocProvider.of<WorkoutCubit>(context).startWorkout(workout) : null),
                          body: ListView.builder(shrinkWrap: true, itemCount: workout.exercises.length, itemBuilder: (BuildContext context, int index) => ListTile(onTap: () => BlocProvider.of<WorkoutCubit>(context).startWorkout(workout, index), visualDensity: const VisualDensity(horizontal: 0, vertical: VisualDensity.minimumDensity), leading: Text(formatTime(workout.exercises[index].prelude, true)), title: Text(workout.exercises[index].title), trailing: Text(formatTime(workout.exercises[index].duration, true))))))
                      .toList()))),
      floatingActionButton: SpeedDial(icon: Icons.add, activeIcon: Icons.close, backgroundColor: Colors.blue, overlayOpacity: 0, children: [
        SpeedDialChild(
            child: const Icon(Icons.edit),
            label: AppLocalizations.of(context).create,
            backgroundColor: Colors.blue,
            onTap: () => showDialog(
                context: context,
                builder: (_) {
                  final controller = TextEditingController();
                  return AlertDialog(
                      content: TextField(
                          controller: controller,
                          decoration: InputDecoration(
                              labelText:
                                  AppLocalizations.of(context).workoutName)),
                      actions: [
                        TextButton(
                            onPressed: () {
                              if (controller.text.isNotEmpty) {
                                BlocProvider.of<WorkoutsCubit>(context)
                                    .createWorkout(controller.text);
                                Navigator.pop(context);
                                BlocProvider.of<WorkoutCubit>(context)
                                    .editWorkout(
                                        BlocProvider.of<WorkoutsCubit>(context)
                                            .state
                                            .first,
                                        0);
                              }
                            },
                            child: Text(AppLocalizations.of(context).create))
                      ]);
                })),
        SpeedDialChild(
            child: const Icon(Icons.upload_file),
            label: AppLocalizations.of(context).import,
            backgroundColor: Colors.blue,
            onTap: () async {
              FilePickerResult result = await FilePicker.platform.pickFiles(
                  type: FileType.custom, allowedExtensions: ['json']);
              if (result != null) {
                BlocProvider.of<WorkoutsCubit>(context).importWorkout(
                    File(result.files.single.path).readAsStringSync());
              }
            })
      ]));
}
